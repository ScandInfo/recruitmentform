﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

// javascript code for index.cshtml. This function was moved because the regex got an error and the program couldnt compile.

var Days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];// index => month [0-11]

$(document).ready(function () {

    $button = $("#submit");

    var option = '<option value="day">Dag</option>';
    var selectedDay = "day";
    for (var i = 1; i <= Days[0]; i++) { //add option days
        if (i <= 9)
            option += '<option value="0' + i + '">0' + i + '</option>';
        else
            option += '<option value="' + i + '">' + i + '</option>';
    }
    $('#day').append(option);
    $('#day').val(selectedDay);

    var option = '<option value="month">Månad</option>';
    var selectedMon = "month";
    for (var i = 1; i <= 12; i++) {
        if (i <= 9)
            option += '<option value="0' + i + '">0' + i + '</option>';
        else
            option += '<option value="' + i + '">' + i + '</option>';
    }
    $('#month').append(option);
    $('#month').val(selectedMon);

    var d = new Date();
    var option = '<option value="year">År</option>';
    selectedYear = "year";
    for (var i = d.getFullYear() - 16; i >= 1930; i--) {// years start i
        option += '<option value="' + i + '">' + i + '</option>';
    }
    $('#year').append(option);
    $('#year').val(selectedYear);
});

function isLeapYear(year) {
    year = parseInt(year);
    if (year % 4 != 0) {
        return false;
    } else if (year % 400 == 0) {
        return true;
    } else if (year % 100 == 0) {
        return false;
    } else {
        return true;
    }
}

function change_year(select) {
    if (isLeapYear($(select).val())) {
        Days[1] = 29;

    }
    else {
        Days[1] = 28;
    }
    if ($("#month").val() == 2) {
        var day = $('#day');
        var val = $(day).val();
        $(day).empty();
        var option = '<option value="day">Dag</option>';
        for (var i = 1; i <= Days[1]; i++) { //add option days
            option += '<option value="' + i + '">' + i + '</option>';
        }
        $(day).append(option);
        if (val > Days[month]) {
            val = 1;
        }
        $(day).val(val);
    }
}

function change_month(select) {
    var day = $('#day');
    var val = $(day).val();
    $(day).empty();
    var option = '<option value="day">Dag</option>';
    var month = parseInt($(select).val()) - 1;
    for (var i = 1; i <= Days[month]; i++) { //add option days
        if (i <= 9)
            option += '<option value="0' + i + '">0' + i + '</option>';
        else
            option += '<option value="' + i + '">' + i + '</option>';
    }
    $(day).append(option);
    if (val > Days[month]) {
        val = 1;
    }
    $(day).val(val);
}

function ValidateEmail() {

    var email = $('#email').val();

    const regexp = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    if (regexp.test(email)) {
        $button.prop('disabled', false);
    } else {
        $button.prop('disabled', true);
    }
}

function ValidateZipCode() {

    // get value.
    var zipcode = $('#zipcode').val();

    zipcode = zipcode.replace(/ /g, '');

    // trimmed value
    $('#zipcode').val(zipcode);

    const regexp = /\b\d{5}\b/g;
    if (regexp.test(zipcode)) {
        $button.prop('disabled', false);
    } else {
        $button.prop('disabled', true);
    }
}

function getUrlParameter() {
    var params = window.location.search;
    var methodElement = document.getElementById("method");
    methodElement.value = params;
    return true;
}