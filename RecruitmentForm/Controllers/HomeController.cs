﻿using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RecruitmentForm.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace RecruitmentForm.Controllers
{

    public class HomeController : Controller
    {
        private string key;
        private string secret;
        private string baseUrl;

        private bool DEV_MODE = false; // DEV_MODE controls urls and key/secret

        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _configuration;

        public HomeController(ILogger<HomeController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;


            if (DEV_MODE)
            {
                baseUrl = "https://cdp.cintworks.net/";
                key = "54434c1b-09b5-4ffd-9464-a16f2bba8c6b";   // Dev-key - Supplied by cint IT.
                secret = "G69qkdMpw6DgF";                       // Dev-secret - Supplied by cint IT.
            } else
            {
                baseUrl = "https://api.cint.com/";
                key = "f863e944-d9ef-466a-838a-34a8b848b457";   // Prod-key - Found via cint-panel -> Engage -> settings -> API
                secret = "XvkSGJVySBwaH";                       // Prod-secret - Found via cint-panel -> Engage ->  settings -> API
            }
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult AddPanelistSuccess()
        {
            return View();
        }

        public IActionResult AddPanelistFailed()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        
        public void GetRequestTest()
        {
            if(DEV_MODE)
            { 
                var url = baseUrl;

                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Basic " + GetBase64Credentials());
                HttpResponseMessage response = client.GetAsync(url).Result;

                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    Debug.WriteLine("{0}", (int)response.StatusCode);
                }

                client.Dispose();
            }
        }

        [HttpPost]
        async public Task<IActionResult> AddPanelist()
        {
            // get post request data.
            var form = HttpContext.Request.Form;

            var dateOfBirth = formatDateOfBirth(form["yyyy"], form["mm"], form["dd"]);

            // A special case of registering a panelist is when doing so for recruitment.
            // If the panelist is created with the status st_nvl,
            // Cint will then email the panelist to validate their email address.
            Panelist panelist = new Panelist(
                "st_nvl",
                form["email"],
                form["gender"],
                dateOfBirth,
                form["zipcode"]
                );
                
            // method of reaching side, sms, web, email etc.
            string method = form["method"];
            if(method.Count() > 0)
                method = method.Contains('=') ? method.Split('=')[1] : "";
            else
                method = "";
            
            if(EmailAddressIsValid(form["email"]))
            {
                // serialize panelist
                var json = JsonConvert.SerializeObject(panelist);

                // encode
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                // url for registering panelists.
                var url = baseUrl + "panels/" + key + "/panelists";

                using var client = new HttpClient();

                // accept header
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // authorization header.
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetBase64Credentials());

                // make request and await response.
                var response = await client.PostAsync(url, data);

                if (response.IsSuccessStatusCode)
                {
                    var test = (int)response.StatusCode;
                    // write prospect to database.
                    AddPanelistToDatabase(panelist, method, test.ToString());

                    // user added.
                    return RedirectToAction("AddPanelistSuccess");
                }
                else
                {
                    var test = (int)response.StatusCode;
                    // write prospect to database.
                    AddPanelistToDatabase(panelist, method, test.ToString());

                    // user not added.
                    var results = response.Content.ReadAsStringAsync().Result; // row kept if developer wants to check response.
                    return RedirectToAction("AddPanelistFailed");
                }
            }
            else
            {
                // write prospect to database.
                AddPanelistToDatabase(panelist, method, "request failed, email invalid");

                // Email invalid in form-object.
                return RedirectToAction("AddPanelistFailed");
            }
        }

        private void AddPanelistToDatabase(Panelist panelist, string method, string CINTResponse)
        {
            var sql = @"INSERT INTO [panelistreg].[dbo].[Prospects] (Email, Added, PostalCode, Method, Gender, CINTResponse, DateOfBirth)
                      values (@Email, @Added, @PostalCode, @Method, @Gender, @CINTResponse, @DateOfBirth)";

            var parameters = new
            {
                Email = panelist.email_address,
                Method = method,
                Added = DateTime.Now,
                Gender = panelist.gender,
                PostalCode = panelist.postal_code,
                CINTResponse = CINTResponse,
                DateOfBirth = panelist.date_of_birth
            };

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Execute(sql, parameters);
            }
        }


        async public void DeletePanelist()
        {
            // A special case of registering a panelist is when doing so for recruitment.
            // If the panelist is created with the status st_nvl,
            // Cint will then email the panelist to validate their email address.

            int panelistId = 779076721;

            var url = baseUrl + "panels/" + key + "/panelists/" + panelistId;

            using var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetBase64Credentials());
            var response = await client.DeleteAsync(url);

            if (response.IsSuccessStatusCode)
            {
                // panelist deleted.
            }
            else
                Debug.WriteLine("{0}", (int)response.StatusCode);

        }

        private bool EmailAddressIsValid(string emailAddress)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(emailAddress);
            if (match.Success)
                return true;
            else
                return false;
        }

        private string GetBase64Credentials()
        {
            string encodedStr = Convert.ToBase64String(Encoding.UTF8.GetBytes(key + ":" + secret));
            return encodedStr;
        }

        private DateTime? formatDateOfBirth(string year, string month, string day)
        {
            DateTime? dateOfBirth = null;

            // if the parameters haven't changed to a numeric value in the form set dateofbirth to empty string. 
            // dateofbirth is not neccessary for cint.
            if(year != "year" & month != "month" & day != "day")
            {
                dateOfBirth = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day));

            }

            return dateOfBirth;
        }
    }
}
