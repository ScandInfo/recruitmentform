﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecruitmentForm.Models
{
    public class Panelist
    {
        public string member_id { get; set; }
        public string status { get; set; }
        public string email_address { get; set; }
        public string gender { get; set; }
        public string postal_code { get; set; }
        public DateTime? date_of_birth { get; set; }
        public string phone_number { get; set; }
        public string street_address { get; set; }
        public string payment_method_id { get; set; }
        public string recruitment_source { get; set; }
        public int[] variables { get; set; }
        public bool tracking_consent { get; set; }

        public Panelist(string status, string emailAddress, string gender, DateTime? dateOfBirth, string postalCode)
        {
            this.status = status;
            email_address = emailAddress;
            this.gender = gender;
            date_of_birth = dateOfBirth;
            postal_code = postalCode;
        }
    }
}
